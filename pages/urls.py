from django.urls import path
from .views import HomePageView, Error404PageView, ContactPageView


urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('not_found/', Error404PageView.as_view(), name='404'),
    path('contact/', ContactPageView.as_view(), name='contact'),   
]