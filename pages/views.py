from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from articles.models import Article
from images.models import Image
from hitcount.views import HitCountDetailView
from articles.views import ArticleDetailView

# Create your views here.
class HomePageView(ListView):
    model = Article
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['articles'] = Article.objects.all()
        context['popular_posts'] = Article.objects.order_by('views')[:3]
        context['latest_posts'] = Article.objects.order_by('-created_date')[:3]
        context['images'] = Image.objects.all()
        return context
    

class Error404PageView(TemplateView):
    template_name = 'pages/404.html'


class ContactPageView(TemplateView):
    template_name = 'pages/contact.html'


