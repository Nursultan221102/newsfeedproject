from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.
class Image(models.Model):
    title = models.CharField('Загаловок картинки', max_length=40)
    img = models.ImageField('Картинка', upload_to='ImageImg')
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,verbose_name='Автор картинки', null=True)
    created_date = models.DateTimeField('Дата публикации', auto_now_add=True, null=True)


    class Meta:
        verbose_name = 'Картинка'
        verbose_name_plural = 'Картинки'
        ordering = ['-created_date']

    def __str__(self):
        return self.title

