from django.urls import path
from .views import ImageDel, ImageDetail, ImageEdit, ImageNew


urlpatterns = [
    path('new_image/', ImageNew.as_view(), name='new_img'),
    path('image_detail/', ImageDetail.as_view(), name='img_detail'),
    path('image_edit/', ImageEdit.as_view(), name='img_edit'),
    path('image_delete/', ImageDel.as_view(), name='img_del'),
]