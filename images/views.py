from django.shortcuts import render
from .models import Image
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.
class ImageNew(generic.CreateView):
    model = Image
    fields = ['title', 'img', 'author']
    template_name = 'images/img_new.html'
    success_url = reverse_lazy('home')


class ImageEdit(generic.UpdateView):
    model = Image
    fields = ['title', 'img']
    template_name = 'images/img_edit.html'
    success_url = reverse_lazy('img_detail')


class ImageDetail(generic.DetailView):
    model = Image
    context_object_name = 'img'
    template_name = 'pages/img_detail.html'


class ImageDel(generic.DeleteView):
    model = Image
    template_name = 'pages/img_del.html'
    success_url = reverse_lazy('home')