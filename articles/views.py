from django.shortcuts import render
from .models import Article, Comment
from django.views import generic
from hitcount.views import HitCountDetailView
from django.urls import reverse_lazy


class ArticleDetailView(HitCountDetailView):
    model = Article
    template_name = 'articles/article_single_page.html'
    context_object_name = 'article'
    slug_field = 'slug'
    count_hit = True


class ArticleNewView(generic.CreateView):
    model = Article
    template_name = 'articles/article_new.html'
    fields = ['title', 'body', 'img', 'category', 'posted_by']
    success_url = reverse_lazy('home')


class ArticleEditView(generic.UpdateView):
    model = Article
    template_name = 'articles/article_edit.html'
    fields = ['title', 'body', 'img']
    success_url = reverse_lazy('home')


class ArticleDelView(generic.DeleteView):
    model = Article
    context_object_name = 'article'
    template_name = 'articles/article_edit.html'
    success_url = reverse_lazy('home')