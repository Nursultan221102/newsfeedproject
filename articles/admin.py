from django.contrib import admin
from .models import Article, Comment

# Register your models here.
@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'posted_by', 'created_date']
    list_display_links = ['id', 'title', 'posted_by', 'created_date']
    ordering = ['created_date']
    search_fields = ['id', 'title', 'created_date',]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ['author', 'comment', 'article', 'created_date']
    list_display_links = ['author', 'created_date', 'article']
    ordering = ['created_date']
    search_fields = ['author', 'article',]
