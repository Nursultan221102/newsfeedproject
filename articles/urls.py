from django.urls import path
from articles.views import ArticleNewView, ArticleDetailView, ArticleEditView, ArticleDelView


urlpatterns = [
    path('new_article/', ArticleNewView.as_view(), name='new_article'),
    path('<slug:slug>/', ArticleDetailView.as_view(), name='article_detail'),
    path('article_edit/<slug:slug>/', ArticleEditView.as_view(), name='article_edit'),
    path('article_delete/<slug:slug>/', ArticleDelView.as_view(), name='article_delete'),
]