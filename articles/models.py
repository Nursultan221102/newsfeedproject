from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from hitcount.models import HitCountMixin, HitCount
from django.contrib.contenttypes.fields import GenericRelation
from six import python_2_unicode_compatible
from django.utils.text import slugify
from django.urls import reverse


GENDER = (
    ('Мужской', 'Мужской'),
    ('Женский', 'Женский'),
)


CATEGORIES = (
    ('Business', 'Business'),
    ('Sport', 'Sport'),
    ('Fashion', 'Fashion'),
    ('Art&Decoration', 'Art&Decoration'),
    ('Games', 'Games'),
    ('Technology', 'Technology'),
    ('Politic', 'Politic'),
    ('Showbusiness', 'Showbusiness'),
    ('Science', 'Science'),
    ('Fairy Tail', 'Fairy Tail'),
    ('None', 'None')
)


@python_2_unicode_compatible
class Article(models.Model):
    title = models.CharField('Загаловок Статьи', max_length=255)
    body = models.TextField('Параграф Статьи')
    img = models.ImageField('Фото Стать', upload_to='ArticleImg')
    category = models.CharField(max_length=50, choices=CATEGORIES, default='no category')
    created_date = models.DateTimeField('Дата публикации Статьи',auto_now_add=True)
    slug = models.SlugField(unique=True, max_length=100, null=True, blank=True)
    views = GenericRelation(HitCount, object_id_field='object_pk',
                                        related_query_name='hit_count_generic_relation')
    posted_by = models.ForeignKey(
                                get_user_model(),
                                on_delete=models.CASCADE,
                                verbose_name='Опубликовал(а) '
                                )


    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ['created_date']

    def __str__(self):
        return self.title
    
    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        else:
            self.slug = slugify(self.title)
        return super(Article, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('article_detail', kwargs={'slug': self.slug})


class Comment(models.Model): 
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name='Статья', related_name='comments')
    comment = models.CharField(max_length=255)
    created_date = models.DateTimeField('Дата публикации Статьи',auto_now_add=True)
    author = models.ForeignKey(
    get_user_model(),
    on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Комментарий Статьи'
        verbose_name_plural = 'Комментарии Статей'
        ordering = ['article', '-created_date']

    def __str__(self):
        return self.comment

    def get_absolute_url(self):
        return reverse('article_list')
