from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import CustomUser


class CustomUserCreationForm(UserCreationForm):
    birthdate = forms.DateField(label='Дата рождения', widget=forms.DateInput(attrs={'type': 'date', 'class': 'birthdate'}))

    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = UserCreationForm.Meta.fields + ('age', 'gender', 'birthdate')
