from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
GENDER = (
    ('Мужской','Мужской'),
    ('Женский','Женский'),
    ('Транс','Транс'),
    ('Не определенный','Не определенный'),
)


class CustomUser(AbstractUser):
    age = models.IntegerField('Возраст', blank=True, null=True,)
    gender = models.CharField('Пол', choices=GENDER, max_length=15)
    birthdate = models.DateField('Дата рождения', blank=True, null=True)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
    
    def __str__(self):
        return self.username